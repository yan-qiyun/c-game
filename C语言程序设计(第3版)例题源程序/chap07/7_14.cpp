/* 【例7-14】进制转换。输入一个以’#’为结束标志的字符串（少于10个字符），滤去所有的非十六进制字符（不分大小写），组成一个新的表示十六进制数字的字符串，输出该字符串并将其转换为十进制数后输出。 */

/* 进制转换 */
# include<stdio.h>
int main(void)
{
    int i,k;
    char hexad[80],str[80];
    long number;

    /* 输入字符串 */
    printf("Enter a string: ");      /* 输入提示 */
    i = 0;
    while((str[i] = getchar( )) != '#')      /* 输入结束符 '#' */
        i++; 
    str[i] = '\0';                /* 将字符串结束符 '\0' 存入数组 */

    /*滤去非16进制字符后生成新字符串hexad */
    k= 0;                     /* k：新字符串hexad的下标 */
    for(i = 0; str[i] != '\0'; i++)
        if(str[i]>='0'&&str[i]<='9'||str[i]>='a'&&str[i]<='f'||str[i]>='A'&&str[i]<='F'){ 
            hexad[k] = str[i];       /* 放入新字符串 */
            k++;         
        }
    hexad[k] = '\0';                 /* 新字符串结束标记 */

    /* 输出十六进制新字符串 */
    printf("New string:");
    for( i= 0; hexad[i] != '\0'; i++) 
        putchar(hexad[i]);
    printf("\n");

    /* 转换为十进制整数 */ 
    number = 0;                  /* 存放十进制数，先清0 */
    for(i = 0; hexad[i] !='\0'; i++){      /* 逐个转换 */
        if(hexad[i] >= '0' &&hexad[i] <= '9')
            number = number * 16 + hexad[i] - '0';
        else if(hexad[i] >= 'A' &&hexad[i] <= 'F')
            number = number * 16 + hexad[i] - 'A' + 10;
        else if(hexad[i] >= 'a' &&hexad[i] <= 'f')
            number = number * 16 + hexad[i] - 'a' + 10;
    }
    printf("Number = %ld\n",number);   /* 输出十进制值 */

    return 0;
}    

