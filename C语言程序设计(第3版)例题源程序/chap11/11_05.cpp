/*【例11-5】输入一些有关颜色的单词，每行一个，以#作为输入结束标志，再以输入的相反次序输出这些单词。其中单词数小于20，每个单词不超过15个字母（用动态分配内存的方法处理多个字符串的输入）。*/

/* 用动态分配内存方法处理多个字符串的输入 */
#include <stdio.h>
#include<stdlib.h>
#include<string.h>
int main(void)
{
    int i, n = 0;
    char *color[20], str[15];
    printf("Please input some words about color:\n");
    scanf("%s", str);
    while(str[0] != '#') {
        color[n] = (char *)malloc(sizeof(char)*(strlen(str)+1));  /* 动态分配 */
        strcpy(color[n], str);       /* 将输入的字符串赋值给动态内存单元 */
	    n++;
        scanf("%s", str);
    }
    printf("These words are:");
    for(i = n-1; i >= 0; i--){          /* 反序输出 */
        printf("%s  ", color[i]);
        free(color[i]);             /* 释放动态内存单元 */
	}

    return 0;
}
