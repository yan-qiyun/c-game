/*【例12-1】有5位学生的计算机等级考试成绩被事先保存在数据文件C:\f12-1.txt(C盘根目录下的文件f12-1.txt，需事先准备好该文件)中，包括学号、姓名和分数，文件内容如下： 
 301101 Zhangwen 91
301102 Chenhui 85
301103 Wangweidong 76
301104 Zhengwei 69
301105 Guowentao 55
请读出文件的所有内容显示到屏幕，并输出平均分。
*/
#include <stdio.h>
#include <stdlib.h>
int main(void)
{
   FILE *fp;                    			/*1.定义文件指针*/
   long num;
   char stname[20];
   int score;
   int i, avg_score = 0;

   if((fp=fopen("c:\\f12-1.txt","r")) == NULL)	/*2.打开文件*/
   {
   		printf("File open error!\n");
   		exit(0);
   }
    										/*3.文件处理（逐个读入和处理数据）*/
   for(i=0;i<5;i++)
   {
	/*从文件读入成绩保存到变量*/
       fscanf(fp,"%ld%s%d",&num,stname,&score);						            
       avg_score += score;    /*统计总分*/
      /*输出成绩到屏幕*/
       printf("%ld	%s %d\n",num,stname,score);  
   }

   /*输出平均分到屏幕*/
	printf("Average score: %d\n", avg_score/5);  			
	if(fclose(fp)){	 						/*4.关闭文件*/
       printf( "Can not close the file!\n" );
       exit(0);
   }
	return 0;
}
